#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Provides a class for BSP packages."""

import hashlib
import os
import shutil
import sys
import tempfile

import status
import subpackage
from core import popen
from core import util

PACKAGE_KEY_TYPE = 'package_type'
PACKAGE_KEY_REMOTE = 'remote'
PACKAGE_KEY_VERSION = 'version'
PACKAGE_KEY_SUBPACKAGES = 'subpackages'

GIT_PACKAGE_TYPE = 'git'
TAR_PACKAGE_TYPE = 'tar'

BSP_PACKAGE_DIR = os.path.expanduser('~/.brillo/.BSPs')
DEFAULT_TAR_FILE = 'tarball.tar.gz'

class Package(object):
  """Class for packages.

  Attributes:
    name - the name of the package.
    remote - the remote location of the package.
    version - the version of the package.
    subpackages - A dictionary of subpackages ({ subpackage_name : Subpackage }).
    directory - the local directory for the package.
  """

  def __init__(self, name, remote, version, subpackages, popener=popen.Popener()):
    self.name = name
    self.remote = remote
    self.version = version
    self.subpackages = subpackages
    self.directory = os.path.join(BSP_PACKAGE_DIR, self.version)
    self._popener = popener

  def _GetSubPackage(self, name):
    subpackage = self.subpackages.get(name)
    if not subpackage:
      raise ValueError, 'Package {0} has no subpackage {1}'.format(self.name, name)
    return subpackage

  def IsDownloaded(self):
    return os.path.isdir(self.directory)

  def SubPackageStatus(self, name, path):
    """Checks the status of a given subpackage.

    Args:
      name - the name of the subpackage to check status of. ValueError if
             self has no subpackage matching the given name.
      path - the path where the subpackage is linked from.
    Returns:
      (status, status_string) pair, where status is:
      status.INVALID - if the given path is a broken symlink.
      status.NOT_INSTALLED - if the given path does not exist in any form.
      status.INCORRECT_VERSION - if the given path is a symlink to the wrong place.
      status.UNRECOGNIZED - if the given path exists but is not a symlink.
      status.INSTALLED - if the given path is a symlink to the correct subdir for the
                         given subpackage.
    """
    subpackage = self._GetSubPackage(name)
    if os.path.exists(path):
      if os.path.islink(path):
        subdir = os.path.join(self.directory, subpackage.subdir)
        if (os.path.exists(subdir) and
            os.path.samefile(subdir, path)):
          result = status.INSTALLED
        else:
          result = status.INCORRECT_VERSION
      else:
        # Uh oh, something unexpected is here.
        result = status.UNRECOGNIZED
    elif os.path.islink(path):
      # Broken symlink.
      result = status.INVALID
    else:
      # Missing completely.
      result = status.NOT_INSTALLED

    return (result, "{0} - {1}".format(name, status.StatusString(result)))

  def RemoveSubPackage(self, name, path):
    """Removes a given subpackage.

    SubPackageStatus of the specified name/path combination
    should be NOT_INSTALLED after this operation.

    Args:
      name - the name of the subpackage to remove. ValueError if
             self has no subpackage matching the given name.
      path - the path where the subpackage is linked from.
    """
    subpackage = self._GetSubPackage(name)

    if os.path.islink(path) or os.path.isfile(path):
      os.remove(path)
    elif os.path.isdir(path):
      shutil.rmtree(path)

  def Download(self, tarball=None, auto_accept=False):
    """Downloads the package files (or just extracts them from a tarball).

    Only downloads if the package is not already downloaded. Checks licenses
    of newly downloaded files.

    Args:
      tarball - (optional) if provided, replaces the download part of tar
                download and skips straight to unzipping.
      auto_accept - (optional) True to automatically accept all licenses.
                    Licenses will still be printed.

    Returns:
      True if download succeeds, False otherwise.
    """
    result = self.IsDownloaded()
    if not result:
      print "Downloading package", self.name
      os.makedirs(self.directory)
      try:
        result = self._TypeSpecificDownload(tarball)
        for subpackage in self.subpackages:
          if not result:
            break
          result = self._CheckSubPackageLicenses(subpackage, auto_accept)
      finally:
        if not result:
          shutil.rmtree(self.directory)
    return result

  def Refresh(self, tarball=None):
    """Refreshes a package.

    Removes (if necessary) and redownloads the package.

    Args:
      tarball - (optional) if provided, replaces the download part of tar
                download and skips straight to unzipping.

    Returns:
      True if refresh succeeds, False otherwise.
    """
    if os.path.isdir(self.directory):
      shutil.rmtree(self.directory)

    return self.Download(tarball)

  def _TypeSpecificDownload(self, tarball=None):
    """Do package type specific downloading.

    See Download for args and return.
    """
    raise NotImplementedError, 'Download function should be overridden in child class.'

  def _CheckSubPackageLicenses(self, name, auto_accept=False):
    """Checks all licenses for a subpackage.

    Args:
      name - the name of hte subpackage to link. ValueError if
             self has no subpackage matching the given name.
      auto_accept - (optional) True to automatically accept all licenses.
                    Licenses will still be printed.

    Returns:
      True if all licenses are accepted, false otherwise.
    """
    if not self.IsDownloaded():
      raise RuntimeError, ('Package {0} must be downloaded before '
                           'subpackage licenses can be checked.'.format(self.name))

    subpackage = self._GetSubPackage(name)
    subdir = os.path.join(self.directory, subpackage.subdir)
    if not os.path.isdir(subdir):
      raise ValueError, ('Package {0} has no subdir {1} '
                         'for subpackage {2}'.format(self.name,
                                                     subpackage.subdir,
                                                     subpackage.name))

    # Check licenses
    for license_file in subpackage.licenses:
      license_path = os.path.join(subdir, license_file)
      # Problem if the license doesn't exist
      if not os.path.isfile(license_path):
        print '{0}.{1} is missing expected license {2}.'.format(self.name,
                                                                subpackage.name,
                                                                license_file)
        return False

      print '{0}.{1} contains a license {2}:'.format(self.name, subpackage.name,
                                                     license_file)

      # Also an issue if the user doesn't accept the license
      if not self._PromptLicense(license_path, auto_accept):
        print 'Failed to install {0}.{1} - license {2} not accepted.'.format(self.name,
                                                                             subpackage.name,
                                                                             license_file)
        return False

    # All licenses accepted
    return True

  def LinkSubPackage(self, name, path):
    """Links a subpackage into the BDK.

    Args:
      name - the name of the subpackage to link. ValueError if
             self has no subpackage matching the given name.
      path - the path where the subpackage should be linked from.
    Returns:
      True if the subpackage is linked, false otherwise.
    """
    if not self.IsDownloaded():
      raise RuntimeError, ('Package {0} must be downloaded before '
                           'subpackages can be linked.'.format(self.name))

    subpackage = self._GetSubPackage(name)
    subdir = os.path.join(self.directory, subpackage.subdir)
    if not os.path.isdir(subdir):
      raise ValueError, ('Package {0} has no subdir {1} '
                         'for subpackage {2}'.format(self.name,
                                                     subpackage.subdir,
                                                     subpackage.name))

    try:
      parent_dir = os.path.dirname(path)
      if parent_dir and not os.path.isdir(parent_dir):
        os.makedirs(parent_dir)
      os.symlink(subdir, path)
      return True
    except OSError as e:
      print 'Trouble linking {0}.{1}: {2}.'.format(self.name, subpackage.name, e)
    return False

  def _PromptLicense(self, license_file, auto_accept=False):
    """Confirms user acceptance of licenses in a downloaded package.

    Args:
      license_file - The license to read.
      auto_accept - Boolean. True to accept the license automatically.

    Returns:
      True if the license is agreed to, False otherwise.
    """
    try:
      with open(license_file) as f:
        print f.read()
    except IOError as e:
      raise IOError, 'Unable to read license file {0} for package {1}: {2}'.format(
        license_file, self.name, e)

    # Get confirmation.
    confirmed = auto_accept or None
    while confirmed is None:
      print 'Do you accept this license (Y/n)? '
      sys.stdout.flush()
      choice = sys.stdin.readline().strip().upper()
      if not choice or choice == 'Y':
        confirmed = True
      elif choice == 'N':
        confirmed = False

    return confirmed

  @classmethod
  def FromDict(cls, dic, name):
    """Create a Package from a dict.

    Args:
      dic - the dictionary to build the package from.
      name - the name of the package.
    """
    package_type = dic[PACKAGE_KEY_TYPE]
    package_subclass = None
    if package_type == GIT_PACKAGE_TYPE:
      package_subclass = GitPackage
    elif package_type == TAR_PACKAGE_TYPE:
      package_subclass = TarPackage
    else:
      raise ValueError, 'Package type must be either {0} or {1}.'.format(
        GIT_PACKAGE_TYPE, TAR_PACKAGE_TYPE)

    subpackages = {}
    for (subpackage_name, data) in dic[PACKAGE_KEY_SUBPACKAGES].iteritems():
      try:
        subpackages[subpackage_name] = subpackage.SubPackage.FromDict(data, subpackage_name)
      except KeyError as e:
        raise KeyError, 'subpackage {0}: {1}'.format(name, e)

    return package_subclass(name,
                            dic[PACKAGE_KEY_REMOTE],
                            dic[PACKAGE_KEY_VERSION],
                            subpackages)

class GitPackage(Package):
  def _TypeSpecificDownload(self, tarball=None):
    """Download a Git repo. See Package.Download."""
    if tarball:
      raise NotImplementedError, ('Initializing Git packages from a tarball '
                                  'is not currently supported.')
    # Setup. Break down version into branch + hash
    # Use rsplit for the off chance ':' is part of the branch name.
    (branch, branch_hash) = self.version.rsplit(':', 1)

    # Check if branch points to hash
    # If so, we can do a shallow clone
    lsremote_process = self._popener.PopenPiped(['git', 'ls-remote',
                                                 self.remote, branch])
    (out, err) = lsremote_process.communicate()
    if lsremote_process.returncode:
      print 'Trouble checking remote {0} for package {1}'.format(self.remote, self.name)
      return False
    if out.startswith(branch_hash):
      shallow = True
      depth = '--depth=1'
    else:
      shallow = False
      depth = '--single-branch'

    # Clone the repo.
    clone_process = self._popener.PopenPiped(['git', 'clone',
                                              depth,
                                              '--branch=' + branch,
                                              self.remote,
                                              self.directory])
    (out, err) = clone_process.communicate()

    if clone_process.returncode:
      print 'Trouble downloading package {0} from {1}'.format(self.name, self.remote)
      return False

    # Checkout the right revision if necessary
    if not shallow:
      checkout_process = self._popener.PopenPiped(['git', 'reset', '--hard',
                                                   branch_hash],
                                                   cwd=self.directory)
      (out, err) = checkout_process.communicate()
      if checkout_process.returncode:
        print 'Trouble checking out correct version of package {0} from {1}'.format(
              self.name, self.remote)
        return False

    # Check that the correct revision was downloaded.
    git_dir = os.path.join(self.directory, '.git')
    hash_check_process = self._popener.PopenPiped(['git',
                                                   '--git-dir=' + git_dir,
                                                   'rev-parse',
                                                   branch])
    (out, err) = hash_check_process.communicate()
    if hash_check_process.returncode:
      print 'Trouble checking version of package {0} from {1}'.format(self.name, self.remote)
      return False
    elif not out.startswith(branch_hash):
      print 'Remote package {0} from {1} is the incorrect version.'.format(self.name, self.remote)
      return False

    # Done. Clean up and return
    shutil.rmtree(git_dir)
    return True

class TarPackage(Package):
  @classmethod
  def GetTarballVersion(cls, tarball_file):
    """Gets the tarball version (sha256) of a tarball file."""
    with open(tarball_file) as tar:
      tar_hash = hashlib.sha256(tar.read()).hexdigest()
    return tar_hash

  def _TypeSpecificDownload(self, tarball_file=None):
    """Download a tarball package. See Package.Download."""
    tarball_provided = (tarball_file != None)
    tar_source = tarball_file if tarball_provided else self.remote # for error messages.

    # Get the tarball if it wasn't provided
    if not tarball_provided:
      # Setup.
      tarball_file = os.path.join(self.directory, DEFAULT_TAR_FILE)

      # Download the tarball.
      fetch_process = self._popener.PopenPiped(['curl',
                                                '-o', tarball_file,
                                                self.remote])
      (out, err) = fetch_process.communicate()
      if fetch_process.returncode:
        print 'Trouble downloading package from', self.remote
        return False

    # Check version of the tarball by using a checksum
    tar_hash = self.GetTarballVersion(tarball_file)
    if not tar_hash.startswith(self.version):
      print 'Tarball for package {0} from {1} does not match expected checksum.'.format(self.name,
                                                                                        tar_source)
      return False

    # Unzip the tarball
    unzip_process = self._popener.PopenPiped(['tar', '-C', self.directory,
                                              '-xzf', tarball_file])
    (out, err) = unzip_process.communicate()
    if unzip_process.returncode:
      print 'Trouble unzipping package {0} from {1}.'.format(self.name, tar_source)
      return False

    # Clean up and return.
    if not tarball_provided:
      os.remove(tarball_file)
    return True
