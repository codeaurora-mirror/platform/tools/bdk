#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Provides a class for BSP manifests."""

import package
import device

MANIFEST_KEY_PACKAGES = 'packages'
MANIFEST_KEY_DEVICES = 'devices'

class Manifest(object):
  """Class for Manifests.

  Manifests are just dumb data.

  Attributes:
    packages - a dictionary of packages ({package_name : Package}).
    devices - a dictionary of devices ({short_name : Device}).
  """
  def __init__(self, packages, devices):
    self.packages = packages
    self.devices = devices

  @classmethod
  def FromDict(cls, dic, bdk=None):
    """Create a Manifest from a dict.

    Args:
      dic - the dictionary to build the Manifest from.
      bdk - (optional) the bdk to work with.
    """
    packages = {}
    devices = {}
    for (package_name, package_spec) in dic[MANIFEST_KEY_PACKAGES].iteritems():
      try:
        packages[package_name] = package.Package.FromDict(package_spec,
                                                          package_name)
      except KeyError as e:
        raise KeyError, 'package {0}: {1}'.format(package_name, e)

    for (short_name, device_spec) in dic[MANIFEST_KEY_DEVICES].iteritems():
      try:
        devices[short_name] = device.Device.FromDict(device_spec,
                                                     packages,
                                                     bdk)
      except KeyError as e:
        raise KeyError, 'device {0}: {1}'.format(short_name, e)

    return cls(packages, devices)
