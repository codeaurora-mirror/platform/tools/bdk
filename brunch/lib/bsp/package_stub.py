#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""A stub for the Package class"""

import status

class StubPackage(object):
  """Replacement for Packages"""
  from_dict_error = None

  def __init__(self, name=None, remote=None, version=None, subpackages=None):
    self.name = name or ''
    self.remote = remote or ''
    self.version = version or ''
    self.subpackages = subpackages or {} # Map of statuses, doubles as iterable of subpackages.
    self.should_remove = []
    self.should_link = {} # Map of { subpackages : post-link status }
    self.should_download = False
    self.should_refresh = False

  def SubPackageStatus(self, name, path):
    # Will error if no such subpackage
    stat = self.subpackages[name]
    return (stat, '{0} - {1}'.format(name, status.StatusString(stat)))

  def RemoveSubPackage(self, name, path):
    if name not in self.should_remove:
      raise ValueError, 'Not supposed to remove {0}.'.format(name)
    # Will error if no such subpackage
    self.subpackages[name] = status.NOT_INSTALLED

  def Download(self, tarball, auto_accept):
    return self.should_download

  def Refresh(self, tarball):
    return self.should_refresh

  def LinkSubPackage(self, subpackage, path, auto_accept=None):
    # Will error if no such subpackage, or if shouldn't link.
    self.subpackages[subpackage] = self.should_link[subpackage]

  @classmethod
  def FromDict(cls, dic, name):
    if from_dict_error:
      raise from_dict_error
    else:
      return cls(name)


class StubTarPackage(StubPackage):
  tarball_version = None

  @classmethod
  def GetTarballVersion(cls, tarball):
    return tarball_version


class StubGitPackage(StubPackage):
  pass
