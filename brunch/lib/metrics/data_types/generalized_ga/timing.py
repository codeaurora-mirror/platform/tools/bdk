#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""A class for hits of the "timing" type."""

import hit


class Timing(hit.Hit):
  """The Timing class can be used to track timings.

  An Timing provides several fields that might be useful for tracking timings.
  What each field should represent is left to the user.

  Attributes:
    category: the Timing category.
    variable: the Timing variable.
    time: the numeric duration timed, in ms.
    label: (optional) a label for the Timing.
    other attributes inherited from base class.
  """

  def __init__(self, meta_data, category, variable, time,
               label=None, custom_dimensions=None, custom_metrics=None):
    super(Timing, self).__init__(meta_data,
                                 self.GA_TIMING_TYPE,
                                 custom_dimensions,
                                 custom_metrics)
    self.category = category
    self.variable = variable
    self.time = time
    self.label = label

  def GetFields(self):
    """See base class."""
    params = super(Timing, self).GetFields()
    params.update({'utc': self.category,
                   'utv': self.variable,
                   'utt': self.time})
    if self.label:
      params['utl'] = self.label
    return params
