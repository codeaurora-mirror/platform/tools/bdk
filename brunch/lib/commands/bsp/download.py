#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Download BSP"""

from bsp import manifest_reader
from bsp import status
from cli import clicommand

class Update(clicommand.Command):
  """Update an existing BSP, or Download a new BSP.

  The software that you are downloading is the property of the software owner
  and is distributed by the software owner. Google is providing the download
  service as a convenience.
  """
  @staticmethod
  def Args(parser):
    parser.add_argument('device', help='Device to download BSP for.')
    parser.add_argument('-a', '--accept_licenses', action='store_true',
                        help='Auto-accept all licenses.')
    parser.add_argument('-e', '--extract_only', nargs='*',
                        help=('List of tarballs to use directly instead of '
                              'downloading from the remote host listed in the manifest'))

  def Run(self, args):
    manifest = manifest_reader.Read(args.manifest, args.bdk)

    device = manifest.devices.get(args.device)
    if not device:
      print 'Unrecognized device name:', args.device
      return 1

    device.Cleanup()

    extract_only = {}
    if args.extract_only:
      extract_only = self.IdentifyTarballs(args.extract_only, device)
      if len(extract_only) != len(args.extract_only):
        print ('Could not identify all provided tarballs '
               '(successfully identified {0}).').format(extract_only.keys())
        return 1

    if device.Install(extract_only, args.accept_licenses):
      return 0
    return 1

  def IdentifyTarballs(self, tarballs, device):
    """Matches a list of tarballs to their corresponding packages.

    Args:
      tarballs - a list of tarball files.
      device - the device to match to packages in.

    Returns:
      A dictionary mapping { package_name : tarball }.
    """

    mapping = {}
    for tarball in tarballs:
      package_name = device.MatchTarball(tarball)
      if package_name:
        mapping[package_name] = tarball

    return mapping

class Download(Update):
  """Alias, see brunch bsp update."""
  pass

class Install(Update):
  """Alias, see brunch bsp update."""
  pass
