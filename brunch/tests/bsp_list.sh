#!/bin/bash

# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

. tests/test_helpers

setup_working_path

echo "Testing brunch bsp list"

${BRUNCH} bsp list -m "${DATA_DIR}/test_manifest.json" > output.txt

grep "Test Board - Not Installed" output.txt
grep "Sharing Board - Not Installed" output.txt
grep "Void Board - Installed" output.txt
